//
//  NetworkService.swift
//  
//
//  Created by Refael Sommer on 15/08/2022.
//

import Foundation

enum NetworkServicePath: String {
    case appleNewsList = "/everything?q=apple"
    case teslaNewsList = "/everything?q=tesla"
}

public class NetworkService: NSObject {
    public static let shared = NetworkService()
    private let token = "91640515a7d84c52a8ea29e44218fcbb"
    private let baseURL = "https://newsapi.org/v2"
    
    func getAppleNewsList(completion: @escaping (_ newsList: [NewsListDTO]) -> Void) {
        getData(path: .appleNewsList) { data, error in
            if let data = data {
                do {
                    let decoder = JSONDecoder()
                    let response = try decoder.decode(NewsListResponse.self, from: data)
                    completion(response.articles ?? [])
                }
                catch let error {
                    print(error)
                }
            }
        }
    }
    
    private func getData(path: NetworkServicePath, completion: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        guard let url = URL(string: baseURL + path.rawValue + "&apiKey=" + token) else {
            completion(nil, nil)
            return
        }
        
        // Create the url request
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print("Error: \(error)")
                completion(nil, error)
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse,
                  (200...299).contains(httpResponse.statusCode) else {
                //print("Error with the response, unexpected status code: \(response)")
                completion(nil, error)
                return
            }
            
            if let data = data {
                completion(data, nil)
            }
        }
        task.resume()
    }
}
