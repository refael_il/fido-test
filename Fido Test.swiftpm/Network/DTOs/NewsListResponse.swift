//
//  NewsListResponse.swift
//  
//
//  Created by Refael Sommer on 15/08/2022.
//

import Foundation

struct NewsListResponse: Codable {
    let status: String
    let totalResults: Int
    let articles: [NewsListDTO]?
}
