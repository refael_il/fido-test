//
//  NewsListDTO.swift
//  
//
//  Created by Refael Sommer on 15/08/2022.
//

import Foundation

struct NewsListDTO: Codable {
    struct source: Codable {
        let id: String?
        let name: String
    }
    
    let author: String?
    let title: String
    let description: String
    let urlToImage: String
    let content: String
    
    var getNewsItem: Models.NewsItem {
        return Models.NewsItem (
            author: author ?? "",
            title: title,
            description: description,
            urlToImage: urlToImage,
            content: content
        )
    }
}
