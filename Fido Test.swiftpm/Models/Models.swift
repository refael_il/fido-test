//
//  Models.swift
//  
//
//  Created by Refael Sommer on 15/08/2022.
//

import Foundation

enum Models { }

extension Models {
    struct NewsItem: Identifiable {
        var id = UUID()
        
        let author: String
        let title: String
        let description: String
        let urlToImage: String
        let content: String
    }
}
