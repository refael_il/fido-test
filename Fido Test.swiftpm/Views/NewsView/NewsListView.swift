import SwiftUI

struct NewsListView: View {
    @ObservedObject var viewModel = ViewModel()
    
    var body: some View {
        if viewModel.newsList.isEmpty {
           ProgressView()
        } else {
            NavigationView {
                List {
                    if viewModel.loading {
                        loadingCell
                    }
                    ForEach(viewModel.newsList) { news in
                        NavigationLink(destination: NewsDetailsView(viewModel: .init(item: news))) {
                            cell(item: news)
                        }
                    }
                }
                .navigationTitle("News List:")
                .onAppear {
                    viewModel.getNewsList()
                }
            }
        }
    }
    
    var loadingCell: some View {
        HStack(alignment: .center) {
                ProgressView()
        }
        .frame(maxWidth: .infinity, alignment: .center)
    }
    
    func cell(item: Models.NewsItem) -> some View {
        VStack {
            HStack {
                Text(item.title)
            }
        }
    }
}
