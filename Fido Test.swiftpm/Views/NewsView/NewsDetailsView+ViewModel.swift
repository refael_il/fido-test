//
//  NewsListView+ViewModel.swift
//  Fido Test
//
//  Created by Refael Sommer on 15/08/2022.
//

import Foundation
import SwiftUI

extension NewsDetailsView {
    final class ViewModel: ObservableObject {
        let item: Models.NewsItem
        
        init(item: Models.NewsItem) {
            self.item = item
        }
    }
}
