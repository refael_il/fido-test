import SwiftUI

struct NewsDetailsView: View {
    let viewModel: ViewModel
    
    var body: some View {
        VStack {
            newsDetails
                .padding([.leading, .trailing], 20)
                .frame(maxHeight: .infinity)
        }
        .background(Color.black)
        .frame(maxHeight: .infinity)
    }
    
    var newsDetails: some View {
        VStack {
            image
            title
            author
            description
            content
        }
        .padding(.all, 25)
        .background(Color.white.opacity(0.6))
    }
    
    var image: some View {
        AsyncImage(url: URL(string: viewModel.item.urlToImage)) { image in
            image
                .resizable()
                .scaledToFill()
                .frame(width: 100, height: 100)
        } placeholder: {
            ProgressView()
        }
        .padding(.bottom, 20)
        .frame(width: 100, height: 100)
    }
    
    var title: some View {
        Text(viewModel.item.title)
            .padding([.leading, .trailing], 25)
            .font(Font.system(size: 20, weight: .bold))
            .multilineTextAlignment(.center)
            .padding(.bottom, 15)
    }
    
    var author: some View {
        Text("Author: " + viewModel.item.author)
            .padding([.leading, .trailing], 25)
            .font(Font.system(size: 14, weight: .medium))
            .multilineTextAlignment(.leading)
            .padding(.bottom, 15)
    }
    
    var description: some View {
        Text(viewModel.item.description)
            .padding([.leading, .trailing], 25)
            .font(Font.system(size: 14, weight: .medium))
            .multilineTextAlignment(.leading)
            .padding(.bottom, 15)
    }
    
    var content: some View {
        Text(viewModel.item.content)
            .padding([.leading, .trailing], 25)
            .font(Font.system(size: 14, weight: .light))
            .multilineTextAlignment(.leading)
    }
}
