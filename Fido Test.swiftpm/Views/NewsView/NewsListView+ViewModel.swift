//
//  NewsListView+ViewModel.swift
//  Fido Test
//
//  Created by Refael Sommer on 15/08/2022.
//

import Foundation
import SwiftUI

extension NewsListView {
    final class ViewModel: ObservableObject {
        @Published var newsList: [Models.NewsItem] = []
        @Published var loading = false
        
        init() {
            NotificationCenter.default.addObserver(
                self,
                selector: #selector(self.appMovedToForground),
                name: UIApplication.willEnterForegroundNotification,
                object: nil
            )
        }
        
        @objc func appMovedToForground() {
            getNewsList()
        }
        
        func getNewsList() {
            loading = true
            NetworkService.shared.getAppleNewsList { list in
                DispatchQueue.main.async {
                    self.newsList = list.map({ dto in
                        dto.getNewsItem
                    })
                    self.loading = false
                }
            }
        }
    }
}
